#!/bin/sh

# Got this from 'xinput --list'
touchpad=16

touchpad_enabled=`xinput list-props $touchpad | grep "Device Enabled" | awk '{print $4}'`

if [ $touchpad_enabled -eq 1 ]; then
    echo "Pad +"
else
    echo "Pad -"
fi

