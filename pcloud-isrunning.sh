#!/bin/bash

# location of logfile. start pcloud as pcloud > logfile
LOGFILE='/tmp/pcloud.log'

# first, is pcloud process running ?
if [ "$(pidof pcloud)" ]; then
    RUNNING=1
else
    RUNNING=0
fi

# status from last line of logfile
LASTLINE=`tail -1 $LOGFILE`

# Lines listing status end like this - "Down: Everything Downloaded| Up: Remaining: 1 files, 6.38MB, status is UPLOADING"
KEYWORD=`$LASTLINE | awk '{print $(NF-2)}'`

echo $KEYWORD

if [[ $KEYWORD == "status" ]]; then
    STATUS=`$LASTLINE | awk '{print $NF}'`
else
    STATUS=$LASTLINE
fi

# if [[ "$LASTLINE" == *"Everything Downloaded| Up: Everything Uploaded"* ]]; then
#     STATUS="Up to date"

# elif [[ "$LASTLINE" == *"Everything Downloaded"* ]]; then
#     STATUS="Uploading"

# elif [[ "$LASTLINE" == *"Everything Uploaded" ]]; then
#     STATUS="Downloading"

# elif [[ "$LASTLINE" == "Found version"* ]]; then
#     echo "Update required"

# else
#     STATUS="Syncing"
# fi


if [[ $RUNNING == 1 ]]; then
    echo " " $STATUS
else
    echo " Not running"
fi


