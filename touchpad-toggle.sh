#!/bin/sh

# Got this from 'xinput --list'
touchpad=16

touchpad_enabled=`xinput list-props $touchpad | grep "Device Enabled" | awk '{print $4}'`

if [ $touchpad_enabled -eq 1 ]; then
    xinput disable 'SynPS/2 Synaptics TouchPad'
else
    xinput enable 'SynPS/2 Synaptics TouchPad'
fi

