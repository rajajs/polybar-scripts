#! /usr/bin/env python
#coding=utf-8

import psutil


logfile = '/tmp/pcloud.log'

# from https://thispointer.com/python-check-if-a-process-is-running-by-name-and-find-its-process-id-pid/
def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


# status from logfile
def current_status(logfile):
    """
    Find the last line in log file which indicates status and extract the status
    Not the most pretty implementation, but works
    """
    # open file and read lines
    try:
        with open(logfile) as fi:
            lines = fi.readlines()
    except IOError:  # file does not exist
        lines = ['No log file']

    # get last status line. this is line with "status is xxx" as last three words
    nlines = len(lines)
    status = "Waiting"
    status_line = ""

    if nlines > 0:
        ind = nlines - 1
        while ind >= 0:
            l = lines[ind]
            if l.split()[-3:-1] == ['status', 'is']:
                status_line = l
                break
            ind -= 1
            
    if status_line != "":
        status = status_line.split()[-1].title()  # in title case
        
    return status

        
RUNNING = checkIfProcessRunning('pcloud')

if RUNNING:
    print " " + current_status(logfile)
else:
    print " " + "Not running"
